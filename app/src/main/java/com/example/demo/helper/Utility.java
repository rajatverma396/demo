package com.example.demo.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.demo.R;
import com.example.demo.models.User;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.typeface.IIcon;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;

import java.util.ArrayList;

import static com.example.demo.activity.Dashboard.ADD_DATA;
import static com.example.demo.activity.Dashboard.DASHBOARD;
import static com.example.demo.activity.Dashboard.LEAD;
import static com.example.demo.activity.Dashboard.LIST;
import static com.example.demo.activity.Dashboard.LOG_OUT;
import static com.example.demo.activity.Dashboard.REGESTRATION;
import static com.example.demo.activity.Dashboard.VIEWS_STATUS;

public class Utility {
    private static TinyDB tinyDB;
    public static final String LOGGED_IN = "LOGGED_IN";
    public static final String USER_INTENT = "USER_INTENT";
    public static final String USER_ID = "USER_ID";


    public static final String ADD_URL = "http://zupascrap.com/lead/rajat/api/add_location.php	";
    public static final String ADD_ITEM_URL = "http://zupascrap.com/lead/rajat/api/add_item.php		";
    public static final String ADD_SOURCE_URL = "http://zupascrap.com/lead/rajat/api/add_source.php	";
    public static final String ADD_STATUS_URL = "http://zupascrap.com/lead/rajat/api/add_status.php	";
    public static final String DELETE_LOCATION_URL = "http://zupascrap.com/lead/rajat/api/delete_location.ph";
    public static final String DELETE_SOURCE_URL = "http://zupascrap.com/lead/rajat/api/delete_source.php";
    public static final String DELETE_STATUS_URL = "http://zupascrap.com/lead/rajat/api/delete_status.php";
    public static final String EDIT_LIST_URL = "http://zupascrap.com/lead/rajat/api/edit_list.php	";
    public static final String EDIT_LOCATION_URL = "http://zupascrap.com/lead/rajat/api/edit_location.php";
    public static final String EDIT_SOURCE_URL = "http://zupascrap.com/lead/rajat/api/edit_source.php	";
    public static final String EDIT_STATUS_URL = "http://zupascrap.com/lead/rajat/api/edit_status.php	";
    public static final String LOGIN_URL = "http://zupascrap.com/lead/rajat/api/login.php		";
    public static final String REGISTER_URL = "http://zupascrap.com/lead/rajat/api/register.php		";

    public static MaterialDialog showLoading(Context context) {
        try {
            return new MaterialDialog.Builder(context)
                    .content("Loading")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
        } catch (Exception e) {
            return null;
        }
    }

    public static MaterialDialog.Builder getDiloag(Context activity, String title, String content) {

        return new MaterialDialog.Builder(activity)
                .title(title)
                .content(content)
                ;


    }

    public static TinyDB getTinyDB(Context ctx) {
        if (tinyDB == null) {
            tinyDB = new TinyDB(ctx);
        }
        return tinyDB;
    }

    public static boolean isDeviceOnline() {

        ConnectivityManager connMgr =
                (ConnectivityManager) AppConfig.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static int getC(int color) {
        return ContextCompat.getColor(AppConfig.getInstance().getBaseContext(), color);
    }

    public static int getBrightColor() {
        try {
            int color = getC(R.color.md_light_blue_A700);
            float[] hsv = new float[3];
            Color.colorToHSV(color, hsv);
            hsv[2] *= 4f; // value component
            return Color.HSVToColor(hsv);
        } catch (Throwable e) {
            return ContextCompat.getColor(AppConfig.getInstance().getBaseContext(), getC(R.color.md_light_blue_50));
        }
    }

    public static IconicsDrawable getIcon(IIcon icon) {
        return new IconicsDrawable(AppConfig.getInstance()).icon(icon);
    }

    public static DrawerBuilder getDrawer(Activity activity, User user, Toolbar toolbar) {
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.gradient)
                .addProfiles(
                        new ProfileDrawerItem().withEmail(user.getEmail()).withIcon(R.drawable.demo).withTextColor(0xFF00E676)
                )
                .build();

        return new DrawerBuilder()
                .withTranslucentStatusBar(true)
                .withActivity(activity)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withAccountHeader(headerResult)
                .withDrawerWidthDp(200)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("DASHBOARD   ").withIdentifier(DASHBOARD).withIcon(getIcon(CommunityMaterial.Icon.cmd_gauge_low).color(getC(R.color.colorAccent))),
                        new PrimaryDrawerItem().withName("ADD LEAD        ").withIdentifier(LEAD).withIcon(getIcon(CommunityMaterial.Icon.cmd_clipboard_account_outline).color(getC(R.color.colorAccent))),
                        new PrimaryDrawerItem().withName("ADD DATA    ").withIdentifier(ADD_DATA).withIcon(getIcon(CommunityMaterial.Icon.cmd_clipboard_plus).color(getC(R.color.colorAccent))),
                        new PrimaryDrawerItem().withName("LIST        ").withIdentifier(LIST).withIcon(getIcon(CommunityMaterial.Icon.cmd_file_document_box_outline).color(getC(R.color.colorAccent))),
                        new PrimaryDrawerItem().withName("VIEWS STATUS").withIdentifier(VIEWS_STATUS).withIcon(getIcon(CommunityMaterial.Icon.cmd_eye_settings_outline).color(getC(R.color.colorAccent))),

                        new PrimaryDrawerItem().withName("REGESTRATION").withIdentifier(REGESTRATION).withIcon(getIcon(CommunityMaterial.Icon.cmd_account_multiple_plus_outline).color(getC(R.color.colorAccent)))
                ).addStickyDrawerItems(
                        new SecondaryDrawerItem().withName("Log Out").withIdentifier(LOG_OUT).withIcon(getIcon(CommunityMaterial.Icon.cmd_exit_to_app).color(getC(R.color.colorAccent))));

    }

    public static ArrayList<String> getSpinnerList() {
        ArrayList<String> categories = new ArrayList<String>();
        categories.add("admin");
        categories.add("hr");
        categories.add("marketing");
        categories.add("Salesman");
        return categories;
    }
}

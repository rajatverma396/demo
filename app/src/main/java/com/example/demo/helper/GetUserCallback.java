package com.example.demo.helper;

import com.example.demo.models.User;

public interface GetUserCallback {
    public abstract void done(String  result);
}

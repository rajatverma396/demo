package com.example.demo.helper;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;



public class AppConfig extends Application {
    private static AppConfig mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public static AppConfig getInstance() {
        return mInstance;
    }


    public TinyDB getTDB() {
        TinyDB tinyDB = Utility.getTinyDB(getInstance());
        if (tinyDB == null)
            tinyDB = new TinyDB(this);
        return tinyDB;
    }
}

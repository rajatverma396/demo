package com.example.demo.server;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.demo.helper.Utility;

import com.example.demo.helper.GetUserCallback;
import com.example.demo.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import static java.lang.System.out;

public class serverrequest {
    private MaterialDialog materialDialog;
    private Context context;
    private static final int connectiontimeout = 1000 * 15;


    public serverrequest(Context context) {
        this.context = context;
    }


    public void fetchuserdatainback(String url, User user, HashMap<String, Object> params, @Nullable GetUserCallback getusercallback) {
        new fetchUserdataasync(url, user, params, getusercallback).execute();
        materialDialog = Utility.showLoading(context);
    }


    public class fetchUserdataasync extends AsyncTask<Void, Void, String> {
        User user;
        GetUserCallback getusercallback;
        HashMap<String, Object> params;
        String url;

        fetchUserdataasync(String url, User user, HashMap<String, Object> params, GetUserCallback getusercallback) {
            this.getusercallback = getusercallback;
            this.user = user;
            this.params = params;
            this.url = url;

        }

        @Override
        protected String doInBackground(Void... voids) {
            RequestHandler requestHandler = new RequestHandler();
            try {
                return requestHandler.sendPostRequest(url, params);
            } catch (Exception e) {
                e.printStackTrace();
                return "UnSuccessfull";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            materialDialog.dismiss();
            if (getusercallback != null)
                getusercallback.done(result);
            super.onPostExecute(result);
        }
    }


}

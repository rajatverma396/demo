package com.example.demo.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;


import com.bumptech.glide.util.Util;
import com.example.demo.R;
import com.example.demo.fragments.Add_Lead;
import com.example.demo.fragments.SignUp_Fragment;
import com.example.demo.helper.AppConfig;
import com.example.demo.helper.TinyDB;
import com.example.demo.helper.Utils;
import com.example.demo.models.LeadModel;
import com.example.demo.models.User;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;
import java.util.List;

import static com.example.demo.helper.Utility.LOGGED_IN;
import static com.example.demo.helper.Utility.USER_ID;
import static com.example.demo.helper.Utility.USER_INTENT;
import static com.example.demo.helper.Utility.getDrawer;

public class Dashboard extends AppCompatActivity {
    public static final int DASHBOARD = 1;
    public static final int LEAD = 2;
    public static final int ADD_DATA = 3;
    public static final int LIST = 4;
    public static final int VIEWS_STATUS = 5;
    public static final int REGESTRATION = 6;
    public static final int LOG_OUT = 7;

    public User user;
    private TinyDB tinyDB;
    private FragmentManager fragmentManager;
    private RecyclerView listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        listItems = findViewById(R.id.list_item);
        toolbar.setTitle("Dashboard");
        tinyDB = AppConfig.getInstance().getTDB();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            user = extras.getParcelable(USER_INTENT);
        } else {
            user = new User();
            user.setEmail(tinyDB.getString(USER_ID, "demo@gmail.com"));
        }
        fragmentManager = getSupportFragmentManager();

        ArrayList<LeadModel> leadList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            LeadModel leadModel = new LeadModel();
            leadModel.setName("demo" + i);
            leadModel.setDate("20/march/201" + i);
            leadModel.setLocation("Noida sec " + i);
            leadModel.setMobile("9" + i + "8998775" + i);
            leadModel.setStatus("Meeting");
            leadModel.setSource("JustDial");
            leadList.add(leadModel);
        }


        ItemAdapter itemAdapter = new ItemAdapter();
        FastAdapter fastAdapter = FastAdapter.with(itemAdapter);
        listItems.setAdapter(fastAdapter);
        itemAdapter.add(leadList);
        listItems.setLayoutManager(new LinearLayoutManager(this));
        listItems.setItemAnimator(new DefaultItemAnimator());


        DrawerBuilder drawerBuilder = getDrawer(this, user, toolbar);
        drawerBuilder.withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                switch ((int) drawerItem.getIdentifier()) {
                    case LOG_OUT:
                        tinyDB.putBoolean(LOGGED_IN, false);
                        tinyDB.putString(USER_ID, "");
                        startActivity(new Intent(Dashboard.this, MainActivity.class));
                        finish();
                        break;
                    case REGESTRATION:
                        fragmentManager
                                .beginTransaction()
                                .setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                .replace(R.id.dashboard, new SignUp_Fragment(true),
                                        Utils.SignUp_Fragment).commit();
                        break;
                    case DASHBOARD:
                        removeFragment("");
                        break;
                    case LEAD:
                        fragmentManager
                                .beginTransaction()
                                .setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                .replace(R.id.dashboard, new Add_Lead(),
                                        Utils.ADDLEAD_Fragment).commit();
                        break;
                }
                return false;
            }
        }).build();

    }

    void removeFragment(String fragmentTag) {

        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();


        for (Fragment frag : fragmentList) {
            if (frag.getTag().equals(fragmentTag)) {
                getSupportFragmentManager().beginTransaction().remove(frag).commit();
                continue;
            } else {
                getSupportFragmentManager().beginTransaction().remove(frag).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {

        Fragment SignUp_Fragment = fragmentManager
                .findFragmentByTag(Utils.SignUp_Fragment);
        if (SignUp_Fragment != null)
            removeFragment(Utils.SignUp_Fragment);
        else
            super.onBackPressed();
    }
}

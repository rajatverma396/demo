package com.example.demo.models;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.demo.R;
import com.example.demo.helper.AppConfig;
import com.example.demo.helper.Utility;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import static com.example.demo.helper.Utility.getIcon;

public class LeadModel extends AbstractItem<LeadModel, LeadModel.ViewHolder> {
    private String name, mobile, location, source, status, imagePath, date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.lead_item;
    }

    protected static class ViewHolder extends FastAdapter.ViewHolder<LeadModel> {
        TextView name, phone, source, location, status, date;
        ImageView userPic, properties;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.fullName);
            phone = itemView.findViewById(R.id.mobile);
            source = itemView.findViewById(R.id.source);
            location = itemView.findViewById(R.id.area);
            status = itemView.findViewById(R.id.status);
            date = itemView.findViewById(R.id.date);
            userPic = itemView.findViewById(R.id.picture);
            properties = itemView.findViewById(R.id.properties);


        }


        @Override
        public void bindView(LeadModel item, List<Object> payloads) {

            name.setText(item.name);
            phone.setText(item.mobile);
            source.setText(item.source);
            location.setText(item.location);
            status.setText(item.status);
            date.setText(item.date);
            properties.setImageDrawable(getIcon(CommunityMaterial.Icon.cmd_dots_vertical).paddingDp(5).color(Utility.getC(R.color.primary)));
            //userPic.setImageDrawable(getIcon(CommunityMaterial.Icon.cmd_account_tie).paddingDp(10).color(Utility.getC(R.color.black)));
            userPic.setImageResource(R.drawable.demo);
           // Glide.with(name.getContext()).load(getIcon(CommunityMaterial.Icon.cmd_account_tie).paddingDp(5)).fitCenter().apply(RequestOptions.circleCropTransform()).into(userPic);


        }

        @Override
        public void unbindView(LeadModel item) {

        }
    }
}

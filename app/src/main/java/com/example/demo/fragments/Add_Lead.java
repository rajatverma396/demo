package com.example.demo.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.demo.R;
import com.example.demo.helper.CustomToast;
import com.example.demo.helper.GetUserCallback;
import com.example.demo.helper.Utility;
import com.example.demo.helper.Utils;
import com.example.demo.models.User;
import com.example.demo.server.serverrequest;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.demo.helper.Utility.getIcon;
import static com.example.demo.helper.Utility.isDeviceOnline;

public class Add_Lead extends Fragment {
    private View view;
    private EditText fullName;
    private EditText emailId;
    private EditText mobileNumber;
    private EditText address;
    private EditText description;
    private TextView datePicker, filePcker;
    private Button done;
    private Spinner state, source, status, city;
    DatePickerDialog datePickerDialog;
    ImageView imageView;
    int request_code = 22;
    Uri selectedImage;
    private static final int REQUEST_READ_PERMISSION = 786;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_lead, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        fullName = view.findViewById(R.id.fullName);
        emailId = view.findViewById(R.id.userEmailId);
        mobileNumber = view.findViewById(R.id.mobile_no);
        address = view.findViewById(R.id.address);
        description = view.findViewById(R.id.description);
        filePcker = view.findViewById(R.id.file_picker);
        datePicker = view.findViewById(R.id.dateofbirth);
        done = view.findViewById(R.id.done);
        imageView = view.findViewById(R.id.image);


        state = view.findViewById(R.id.state);
        source = view.findViewById(R.id.source);
        status = view.findViewById(R.id.status);
        city = view.findViewById(R.id.city);


        description.setCompoundDrawablesWithIntrinsicBounds(getIcon(CommunityMaterial.Icon.cmd_account_badge_outline).sizeDp(20).color(Utility.getC(R.color.white_greyish)), null, null, null);
        datePicker.setCompoundDrawablesWithIntrinsicBounds(getIcon(CommunityMaterial.Icon.cmd_calendar_today).sizeDp(20).color(Utility.getC(R.color.white_greyish)), null, null, null);
        filePcker.setCompoundDrawablesWithIntrinsicBounds(getIcon(CommunityMaterial.Icon.cmd_filmstrip).sizeDp(20).color(Utility.getC(R.color.white_greyish)), null, null, null);

        ArrayList<String> city = new ArrayList<>();
        city.add("Select City");
        city.add("Delhi");
        city.add("Noida");
        city.add("Faridabad");
        city.add("Gurugram");
        city.add("Gaziabad");
        ArrayList<String> state = new ArrayList<>();
        state.add("Select State");
        state.add("Delhi");
        state.add("UP");
        state.add("Haryana");
        state.add("Punjab");
        ArrayList<String> source = new ArrayList<>();
        source.add("Select Source");
        source.add("Website");
        source.add("Market Field");
        source.add("Calling ");
        source.add("JustDial");
        ArrayList<String> status = new ArrayList<>();
        status.add("Select Status ");
        status.add("Hold ");
        status.add("Rejected ");
        status.add("Final ");
        status.add("job call ");
        status.add("Forward to Sonu ");
        status.add("Ringing/not picup ");
        status.add("Fake enquiry ");


        ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_layout, city);
        ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_layout, state);
        ArrayAdapter<String> sourceAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_layout, source);
        ArrayAdapter<String> statusAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_layout, status);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sourceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.city.setAdapter(cityAdapter);
        this.state.setAdapter(stateAdapter);
        this.source.setAdapter(sourceAdapter);
        this.status.setAdapter(statusAdapter);

        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        datePicker.setText(month + "-" + dayOfMonth + "-" + year);
                    }
                }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation();
            }
        });
        filePcker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStoragePermissionGranted();
            }
        });
    }


    public void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (this.getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                filePicker();

            } else {
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_PERMISSION);
            }
        }
    }

    private void filePicker() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, request_code);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_READ_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            filePicker();
        }
    }

    private void checkValidation() {


        String fullName = this.fullName.getText().toString();
        String emailId = this.emailId.getText().toString();
        String mobileNumber = this.mobileNumber.getText().toString();
        String address = this.address.getText().toString();
        String description = this.description.getText().toString();


        Pattern p = Pattern.compile(Utils.regEx);
        Matcher m = p.matcher(emailId);


        if (fullName.equals("") || fullName.length() == 0
                || emailId.equals("") || emailId.length() == 0
                || mobileNumber.equals("") || mobileNumber.length() == 0 || mobileNumber.length() < 10
                || address.equals("")
                || description.equals("")
                || city.getSelectedItemPosition() == 0 || state.getSelectedItemPosition() == 0
                || source.getSelectedItemPosition() == 0 || status.getSelectedItemPosition() == 0)

            new CustomToast().Show_Toast(getActivity(), view,
                    "All fields are required.");


        else if (!m.find())
            new CustomToast().Show_Toast(getActivity(), view,
                    "Your Email Id is Invalid.");


        else {

            if (isDeviceOnline()) {
                User user = new User();
                user.setUsername(fullName);
                user.setEmail(emailId);
                user.setDescription(description);
                user.setAddress(address);
                user.setDateOfBirth(datePicker.getText().toString());
                user.setMobileNumber(mobileNumber);
                user.setSource(source.getSelectedItem().toString());
                user.setState(state.getSelectedItem().toString());
                user.setStatus(status.getSelectedItem().toString());
                user.setCity(city.getSelectedItem().toString());
                user.setImagePath(selectedImage);
                authenticate(user);
            } else {
                new CustomToast().Show_Toast(getActivity(), view,
                        " Please,Connect to Network and try Again ");
            }

        }
    }

    private void authenticate(User user) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", user.getUsername());
        params.put("emailid", user.getEmail());
        params.put("despt", user.getDescription());
        params.put("townname", user.getAddress());
        params.put("datepicker", user.getDateOfBirth());
        params.put("mobile", user.getMobileNumber());
        params.put("source", user.getSource());
        params.put("status", user.getStatus());
        params.put("state", user.getState());
        params.put("location", user.getCity());
       // params.put("image", user.getImagePath());
        serverrequest serverrequest = new serverrequest(getContext());
        serverrequest.fetchuserdatainback(Utility.ADD_ITEM_URL, user, params, new GetUserCallback() {
            @Override
            public void done(String result) {
                Toast.makeText(getActivity(), result + " Success", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == request_code)
            if (resultCode == Activity.RESULT_OK) {
                selectedImage = data.getData();
                Glide.with(getContext()).load(selectedImage).into(imageView);


            }
    }
}
package com.example.demo.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demo.R;
import com.example.demo.helper.CustomToast;
import com.example.demo.activity.MainActivity;
import com.example.demo.helper.GetUserCallback;
import com.example.demo.helper.Utils;
import com.example.demo.models.User;
import com.example.demo.server.serverrequest;

import static com.example.demo.helper.Utility.REGISTER_URL;
import static com.example.demo.helper.Utility.getSpinnerList;
import static com.example.demo.helper.Utility.isDeviceOnline;

public class SignUp_Fragment extends Fragment implements OnClickListener, AdapterView.OnItemSelectedListener {
    private View view;
    private EditText fullName, emailId, mobileNumber, location, password, confirmPassword;
    private TextView login;
    private Button signUpButton;
    private CheckBox terms_conditions;
    private Spinner spinner;

    private boolean dashboardFlag = false;

    public SignUp_Fragment(boolean dashboardFlag) {
        this.dashboardFlag = dashboardFlag;
    }

    public SignUp_Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.signup_layout, container, false);
        initViews();
        setListeners();
        return view;
    }


    private void initViews() {
        fullName = view.findViewById(R.id.fullName);
        emailId = view.findViewById(R.id.userEmailId);
        spinner = view.findViewById(R.id.spinner);
        password = view.findViewById(R.id.password);
        confirmPassword = view.findViewById(R.id.confirmPassword);
        signUpButton = view.findViewById(R.id.signUpBtn);
        login = view.findViewById(R.id.already_user);
        terms_conditions = view.findViewById(R.id.terms_conditions);
        if (dashboardFlag) {
            login.setVisibility(View.GONE);
        }
        spinner.setOnItemSelectedListener(this);


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, getSpinnerList());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);


        @SuppressLint("ResourceType") XmlResourceParser xrp = getResources().getXml(R.drawable.text_selector);
        try {
            ColorStateList csl = ColorStateList.createFromXml(getResources(),
                    xrp);

            login.setTextColor(csl);
            terms_conditions.setTextColor(csl);
        } catch (Exception e) {
        }
    }

    // Set Listeners
    private void setListeners() {
        signUpButton.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpBtn:


                checkValidation();
                break;

            case R.id.already_user:


                ((MainActivity) getActivity()).replaceLoginFragment();
                break;
        }

    }

    // Check Validation Method
    private void checkValidation() {


        String getFullName = fullName.getText().toString();
        String getEmailId = emailId.getText().toString();
        String getPassword = password.getText().toString();
        String getConfirmPassword = confirmPassword.getText().toString();


        Pattern p = Pattern.compile(Utils.regEx);
        Matcher m = p.matcher(getEmailId);


        if (getFullName.equals("") || getFullName.length() == 0
                || getEmailId.equals("") || getEmailId.length() == 0
                || getPassword.equals("") || getPassword.length() == 0
                || getConfirmPassword.equals("")
                || getConfirmPassword.length() == 0)

            new CustomToast().Show_Toast(getActivity(), view,
                    "All fields are required.");


        else if (!m.find())
            new CustomToast().Show_Toast(getActivity(), view,
                    "Your Email Id is Invalid.");


        else if (!getConfirmPassword.equals(getPassword))
            new CustomToast().Show_Toast(getActivity(), view,
                    "Both password doesn't match.");


        else {

            if (isDeviceOnline()) {
                User user = new User();
                user.setUsername(getFullName);
                user.setEmail(getEmailId);
                user.setPassword(getPassword);
                user.setAcountType(spinner.getSelectedItem().toString());
                authenticate(user);
            } else {
                new CustomToast().Show_Toast(getActivity(), view,
                        " Please,Connect to Network and try Again ");
            }

        }
    }

    private void authenticate(User user) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("username", user.getUsername());
        params.put("email", user.getEmail());
        params.put("password", user.getPassword());
        params.put("usertype", user.getAcountType());
        serverrequest serverrequest = new serverrequest(getContext());
        serverrequest.fetchuserdatainback(REGISTER_URL, user, params, new GetUserCallback() {
            @Override
            public void done(String result) {
                Toast.makeText(getActivity(), "SignUp Success", Toast.LENGTH_LONG).show();
                if (!dashboardFlag)
                    if (((MainActivity) getActivity()) != null) {
                        ((MainActivity) getActivity()).replaceLoginFragment();
                    }
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

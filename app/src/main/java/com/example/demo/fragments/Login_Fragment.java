package com.example.demo.fragments;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demo.R;
import com.example.demo.activity.Dashboard;
import com.example.demo.helper.CustomToast;
import com.example.demo.helper.Utils;
import com.example.demo.helper.AppConfig;
import com.example.demo.helper.GetUserCallback;
import com.example.demo.helper.TinyDB;
import com.example.demo.models.User;
import com.example.demo.server.serverrequest;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.demo.helper.Utility.LOGGED_IN;
import static com.example.demo.helper.Utility.LOGIN_URL;
import static com.example.demo.helper.Utility.USER_ID;
import static com.example.demo.helper.Utility.USER_INTENT;
import static com.example.demo.helper.Utility.isDeviceOnline;

public class Login_Fragment extends Fragment implements OnClickListener {
    private static View view;

    private EditText emailid, password;
    private Button loginButton;
    private TextView forgotPassword, signUp;
    private CheckBox show_hide_password;
    private LinearLayout loginLayout;
    private Animation shakeAnimation;
    private FragmentManager fragmentManager;
    private TinyDB tinyDB;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        tinyDB = AppConfig.getInstance().getTDB();

        view = inflater.inflate(R.layout.login_layout, container, false);
        initViews();
        setListeners();
        return view;
    }

    // Initiate Views
    private void initViews() {

        fragmentManager = getActivity().getSupportFragmentManager();
        emailid = view.findViewById(R.id.login_emailid);
        password = view.findViewById(R.id.login_password);
        loginButton = view.findViewById(R.id.loginBtn);
        forgotPassword = view.findViewById(R.id.forgot_password);
        signUp = view.findViewById(R.id.createAccount);
        show_hide_password = view.findViewById(R.id.show_hide_password);
        loginLayout = view.findViewById(R.id.login_layout);


        shakeAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.shake);


        @SuppressLint("ResourceType") XmlResourceParser xrp = getResources().getXml(R.drawable.text_selector);
        try {
            ColorStateList csl = ColorStateList.createFromXml(getResources(),
                    xrp);

            forgotPassword.setTextColor(csl);
            show_hide_password.setTextColor(csl);
            signUp.setTextColor(csl);
        } catch (Exception e) {
        }
    }

    // Set Listeners
    private void setListeners() {
        loginButton.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        signUp.setOnClickListener(this);

        // Set check listener over checkbox for showing and hiding password
        show_hide_password
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton button,
                                                 boolean isChecked) {


                        if (isChecked) {

                            show_hide_password.setText(R.string.hide_pwd);// change

                            password.setInputType(InputType.TYPE_CLASS_TEXT);
                            password.setTransformationMethod(HideReturnsTransformationMethod
                                    .getInstance());// show password
                        } else {
                            show_hide_password.setText(R.string.show_pwd);// change

                            password.setInputType(InputType.TYPE_CLASS_TEXT
                                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            password.setTransformationMethod(PasswordTransformationMethod
                                    .getInstance());

                        }

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBtn:
                checkValidation();
                break;

            case R.id.forgot_password:


                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                        .replace(R.id.frameContainer,
                                new ForgotPassword_Fragment(),
                                Utils.ForgotPassword_Fragment).commit();
                break;
            case R.id.createAccount:


                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                        .replace(R.id.frameContainer, new SignUp_Fragment(),
                                Utils.SignUp_Fragment).commit();
                break;
        }

    }


    private void checkValidation() {

        String getEmailId = emailid.getText().toString();
        String getPassword = password.getText().toString();


        Pattern p = Pattern.compile(Utils.regEx);

        Matcher m = p.matcher(getEmailId);

        if (getEmailId.equals("") || getEmailId.length() == 0
                || getPassword.equals("") || getPassword.length() == 0) {
            loginLayout.startAnimation(shakeAnimation);
            new CustomToast().Show_Toast(getActivity(), view,
                    "Enter both credentials.");

        }
        // Check if email id is valid or not
        else if (!m.find())
            new CustomToast().Show_Toast(getActivity(), view,
                    "Your Email Id is Invalid.");
            // Else do login and do your stuff
        else {
            User user = new User(getEmailId, getPassword);
            if (isDeviceOnline()) {
                authenticate(user);
            } else {
                new CustomToast().Show_Toast(getActivity(), view,
                        "Please,Connect to Network and try Agains.");
            }


        }

    }

    private void authenticate(User user) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", user.getUsername());
        params.put("password", user.getPassword());
        serverrequest serverrequest = new serverrequest(getContext());
        serverrequest.fetchuserdatainback(LOGIN_URL, user, params, new GetUserCallback() {
            @Override
            public void done(String result) {
                User retureneduser = null;
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    retureneduser = new User();
                    retureneduser.setUsername(jsonObject.getString("name"));
                    retureneduser.setEmail(jsonObject.getString("email"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    retureneduser = null;

                }

                if (retureneduser == null) {
                    new CustomToast().Show_Toast(getActivity(), view,
                            "Login Failed");
                    loginLayout.startAnimation(shakeAnimation);
                    //  getDiloag(getContext(), "Login Failed", "").positiveText("ok").show();
                } else {
                    Toast.makeText(getContext(), "Login Success", Toast.LENGTH_SHORT).show();
                    tinyDB.putBoolean(LOGGED_IN, true);
                    tinyDB.putString(USER_ID, retureneduser.getEmail());
                    Intent intent = new Intent(getContext(), Dashboard.class);
                    intent.putExtra(USER_INTENT, retureneduser);
                    startActivity(intent);
                }
            }
        });

    }

}
